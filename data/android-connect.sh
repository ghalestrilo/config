#!/bin/bash

#Run this to find out connection
#adb tcpip 5555
#adb shell ifconfig
#adb shell ip address show

connection=$(iwgetid -r)

case $connection in
"IW-Backup") adb connect "192.168.15.4";;
*) echo "IP unknown for connection: $connection" 
esac