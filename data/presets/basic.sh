#!/bin/bash

# Basic Preset
# Minimal tools for everiday live

# Programs
export MUSESCORE=false
export REAPER=false
export FRANZ=true
export KDENLIVE=false
export VIVALDI=true
export SYNCTHING=true

# Tools
export NODE=false
export NPM=false
export CURL=true
export GIT=true
export HTTP=false
export NEOFETCH=true
export SCIM=true
export DOCKER=false
export BEETS=false

# Editors
export MICRO=false
export NEOVIM=false
export CODE=true
export THEFUCK=true
export SHELLCHECK=true
export ANDROID=false

# Languages
export CRYSTAL=true
export CLOJURE=true
export SCHEME=false
export HASKELL=false
export FORTRAN=false
export ELM=true

# Libraries
export SFML=true
export CRSFML=true
export AMBER=false
export SDL=true
export YAML=true
export RAYLIB=true

# Folders
declare -a folders
folders=()