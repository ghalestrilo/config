#!/bin/bash

# Rhino Preset
# Focused towards music production

# Programs
export MUSESCORE=true
export REAPER=true
export FRANZ=true
export KDENLIVE=true
export VIVALDI=true
export SYNCTHING=true

# Tools
export NODE=true
export NPM=true
export CURL=true
export GIT=true
export HTTP=true
export NEOFETCH=true
export SCIM=true
export DOCKER=false
export BEETS=false

# Editors
export MICRO=false
export NEOVIM=false
export CODE=true
export THEFUCK=true
export SHELLCHECK=true

# Languages
export CRYSTAL=true
export CLOJURE=true
export SCHEME=false
export HASKELL=false
export FORTRAN=false
export ELM=true
export ANDROID=false

# Libraries
export SFML=true
export CRSFML=true
export AMBER=false
export SDL=true
export YAML=true
export RAYLIB=true

# Folders
declare -a folders
folders=(
    git
	comp
	proj
	books
	doc
    music
)