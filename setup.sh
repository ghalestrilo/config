#!/bin/bash
# Run as Root

#https://robots.thoughtbot.com/shell-script-suggestions-for-speedy-setups

# Essential Definitions for script

. scripts/dirs.sh
. $scripts/defs.sh

runscript(){
  section "$2"
  . $scripts/$1.sh
  saydone
}


# Actual running script

# 1: Read User Options
runscript options "Reading user options"

# 2: Add Repos
runscript repos    "Setting ppa repositories"

# 3: Install Programs
runscript programs "Installing requested programs"

# 4: Copy Config Files
runscript configs  "Copying configuration files"

# 5: Global npm packages
# runscript npm      "Setting up npm packages"

# 7: Log and save errors, if there are
if [ $errcount -gt 0 ]; then
  mv $errlog .

  printf "\nErrors detected: $RED $errcount $NO_COLOR\n"
  printf "run ${YELLOW}more errlog${NO_COLOR} to know more\n"
fi

# 8: Cleanup
rm -rf temp/*
