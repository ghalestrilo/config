VIMRC = ~/.config/nvim/init.vim

push:
	git add . ;\
	git commit -m "autopush" ;\
	git push ;


configure:
	bash ./setup.sh


save:
	bash ./scripts/updateconfig.sh reverse

load:
	bash ./scripts/updateconfig.sh

backup:
	bash ./scripts/backup.sh

setup:
	bash ./scripts/backup.sh reverse