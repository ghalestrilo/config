#!/bin/bash

# Move Config
# $1: source directory, if not reversed
# $2: output directory, if not reversed
# $4: filename
moveconfig(){
  if $reverse
  then stepcopy "$2/$3" "$1"
  else stepcopy "$1/$3" "$2"
  fi
}

# Configure
configure(){
  sayname "$1"
  moveconfig "$data" "$2" "$3"
  sayok
}

# MuseScore
configure "musescore"   "$musescoreconfig" "MuseScore2.ini"

# Kakoune
configure "kakoune"     "$kakouneconfig"   "kakrc"

# Kakoune Language Service Provider
configure "kak-lsp"     "$kaklspconfig"    "kak-lsp.toml"

# Task Warrior
configure "taskwarrior" "$HOME"            ".taskrc"

# OMZSH
configure "oh my zsh"   "$HOME"            ".zshrc"
configure "zsh theme"   "$HOME/.oh-my-zsh/themes" "lalis.zsh-theme"

# i3
configure "i3"          "$i3config"        "config"

# code
sayname "code"
moveconfig "$data/code" "$codeconfig" "settings.json"
moveconfig "$data/code" "$codeconfig" "keybindings.json"
sayok

# supercollider
sayname "supercollider"
moveconfig "$data/supercollider" "$supercolliderconfig" "startup.scd"
moveconfig "$data/supercollider" "$supercolliderconfig" "forward_osc.scd"
sayok

# tidalcycles
sayname "tidal"
moveconfig "$data/tidal" "$tidalconfig" "boot.hs"
sayok


# Vim
sayname "vim"
moveconfig "$data" "$nvimconfig" "init.vim"
vim_plug_url=https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim_autoload=$nvimconfig/autoload
if ! $reverse; then
	stepcurl $vim_plug_url $vim_autoload/plug.vim
fi
sayok
