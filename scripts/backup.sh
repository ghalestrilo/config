#!/bin/bash

. ./scripts/dirs.sh
. ./scripts/defs.sh
. ./scripts/flags.sh "$@"
. ./scripts/options.sh "$@"

# Default directories
homedir="$HOME"
backupdir="/media/lalis/jupitu"

# COMMON => DEFS
movedata(){
  if $reverse
  then stepcopy "$2/$3" "$1"
  else stepcopy "$1/$3" "$2"
  fi
}

# Configure
backup(){
	sayname "$1"
	movedata "$homedir" "$backupdir" "$1"
	sayok
}
echo
echo

if [ "$reverse" = "true" ];
then yellow " Updating configuration files\n"
else yellow " Backing up folders\n"
fi

if [ ${#folders[@]} = 0 ]
then echo "warn: not copying folders because none are listed in options file"
else
	for folder in "${folders[@]}"; do
		backup $folder
	done
fi
