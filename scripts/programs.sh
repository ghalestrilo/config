#!/bin/bash


steptar(){
  printf "${CURRENT_TEXT}"
  tar xzf $1 -C $2
}

if $MUSESCORE   ;then tryapt musescore   "musescore"   ; fi
if $FRANZ       ;then tryapt franz       "franz"       ; fi
if $KDENLIVE    ;then tryapt kdenlive    "kdenlive"    ; fi
# if $VIVALDI     ;then tryapt vivaldi     "vivaldi"     ; fi
# if $CODE        ;then tryapt code        "code"        ; fi
if $SYNCTHING   ;then tryapt syncthing   "syncthing"   ; fi

# Vivaldi
if $vivaldi && ! command -v vivaldi > $null; then
  sayname "vivaldi"
  vivaldi_deb=$temp/vivaldi.deb
  vivaldi_stable="https://downloads.vivaldi.com/stable/vivaldi-stable_1.15.1147.64-1_amd64.deb"
  stepcurl "$vivaldi_stable" "$vivaldi_deb"
  stepdpkg "$vivaldi_deb"


  # Video Codecs
  stepapt chromium-codecs-ffmpeg-extra
  # subprint "apt-get installing chromium-codecs-ffmpeg-extra"
  # apt-get install chromium-codecs-ffmpeg-extra -y > $null

  sayok
fi

# Command Line Tools
if $GIT        ;then tryapt git        "git"        ; fi

if $SHELLCHECK ;then tryapt shellcheck "shellcheck" ; fi
if $CURL       ;then tryapt curl       "curl"       ; fi
#if $MICRO      ;then tryapt micro      "micro"      ; fi
if $NEOVIM     ;then tryapt neovim     "neovim"     ; fi
if $HTTP       ;then tryapt httpie     "httpie"     ; fi
if $CLOC       ;then tryapt cloc       "cloc"       ; fi
if $NEOFETCH   ;then tryapt neofetch   "neofetch"   ; fi

if $ZSH; then
	tryapt zsh "zsh"
	chsh -s /usr/local/zsh
fi

if $SCIM; then 
	sayname "sc-im"
	tryapt "bison"
	source="https://github.com/andmarti1424/sc-im"
	cwd=$PWD

	cd $temp   && git clone "$source"
	cd "sc-im" && make -C src
	cd "src"   && sudo make install

	cd $cwd
	sayok
fi

if $MICRO && ! command -v "micro"; then 
	sayname "micro"
	source="https://getmic.ro"
	script="$temp/micro.sh"

	stepcurl "$source" "$script"
	sh $script > $tmplog

	mv micro /usr/bin
	sayok
fi

if $OMZSH; then
  sayname "oh my zhs"
  source="https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh"
  script="$temp/omzsh.sh"

  stepcurl "$source" "$script"
  sh $script
  sayok
fi

# The Fuck
if $THEFUCK && ! command -v "fuck"; then
  sayname "thefuck"
  stepapt "python3-dev python3-pip"
  subprint "pip installing thefuck"
  pip3 install thefuck -y > $null
  sayok
fi

# Kakoune
if $KAKOUNE && ! command -v "kak"; then
  kakounerepo=https://github.com/mawww/kakoune.git

  sayname "kakoune"
  stepapt "libncursesw5-dev asciidoc"

  git clone $kakounerepo $temp/kakoune && cd $temp/kakoune/src || exit
  PREFIX=$HOME/.local
  if ! command -v pkg-config; then
      # apt-get install pkg-config -y > $null
      stepapt "pkg-config"
  fi
  make > $null
  make install > $null
  cd "$dir"
  

  # Configure kak-lsp
  if $LSP; then
    kak_lsp="https://github.com/ul/kak-lsp/releases/download/v5.3.0/kak-lsp-v5.3.0-x86_64-unknown-linux-musl.tar.gz"
    kak_lsp_tarball="$temp/kak_lsp.tar.gz"

    stepcurl $kak_lsp $kak_lsp_tarball
    tar xzvf $kak_lsp_tarball

    # CHECK IF ACTUALLY THERE

    # replace `~/.local/bin/` with something on your `$PATH`
    stepcopy "$temp/kak-lsp" "/usr/bin/"

    # Taken care of during config stage
    # stepcopy "$temp/kak-lsp.toml" "$HOME/.config/kak-lsp/"
  fi
  sayok
fi
saydone


# Environment
section "Setting up environment"

if $CRYSTAL \
|| $CRSFML \
|| $AMBER   ;then tryapt crystal 		  "crystal"          ;fi 
if $HASKELL ;then tryapt ghc 				  "haskell (ghc)"    ;fi 
if $SCHEME  ;then tryapt chicken-bin  "scheme (chicken)" ;fi 
if $NODE    ;then tryapt nodejs 		  "node"             ;fi 
if $YAML    ;then tryapt libyaml-dev  "libyaml-dev"      ;fi


if $TIDAL; then
  tryapt haskell-platform "haskell"
  tryapt supercollider    "supercollider" 
  tryapt sc3-plugins      "sc3-plugins"   
  tryapt cabal-install    "cabal-install" 

  cabal update
  cabal install tidal
fi

if $AMBER; then
	sudo apt-get install libreadline-dev libsqlite3-dev libpq-dev libmysqlclient-dev libssl-dev libyaml-dev
	curl -L https://github.com/amberframework/amber/archive/stable.tar.gz | tar xz
	cd amber-stable/
	shards install
	make install
fi

if $DOCKER; then
	tryapt "apt-transport-https"
	tryapt "ca-certificates"
	if ! command -v "curl"; then
		tryapt "curl"
	fi

	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
	sudo apt update
	sudo apt install docker-ce
fi

if $CLOJURE && ! command -v "lein"; then
    script=$temp/clojure-install.sh
    source="https://download.clojure.org/install/linux-install-1.9.0.375.sh"

    sayname "clojure"
    stepapt default-jre

    stepcurl $source $script 
    chmod +x $script 
    ./$script
    sudo chmod a+x $temp/lein.sh
    stepcopy "./config/lein.sh" "/usr/local/bin/"
    sayok

    stepapt "leiningen"
fi

if $OVERTONE && false; then
  # Magic line that solvers AWTError
  sudo sed -i -e '/^assistive_technologies=/s/^/#/' /etc/java-*-openjdk/accessibility.properties
fi


if $ANDROID; then
	STUDIODIR="/usr/local/android-studio"
	JDK8DIR="/usr/lib/jdk1.8.0"	
	sayname "android-studio"
	

	if [ -d $JDK8DIR ]; then
		subprint "${YELLOW}preious configuration detected ${NO_COLOR}- refusing to add PATH lines to ~/.profile"
	else
		# Download Java
		#stepcurl
		#steptar
		stepcopy "$temp/jdk1.8.0.0_181/*" "$JDK8DIR"
		echo export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_151.jdk/Contents/Home
		update-alternatives --install "/usr/bin/java" "java" "$JDK8DIR/bin/java" 1
		update-alternatives --install "/usr/bin/javac" "javac" "$JDK8DIR/bin/javac" 1
		update-alternatives --install "/usr/bin/javaws" "javaws" "$JDK8DIR/bin/javaws" 1

		update-alternatives --set java $JDK8DIR/bin/java
		update-alternatives --set javac $JDK8DIR/bin/javac

	fi

		

	#1. Unpack the .zip file you downloaded to an appropriate location for your applications, such as within /usr/local/ for your user profile, or /opt/ for shared users.
	#2. To launch Android Studio, open a terminal, navigate to the android-studio/bin/ directory, and execute studio.sh.
	#3. Select whether you want to import previous Android Studio settings or not, then click OK.
	#4. The Android Studio Setup Wizard guides you through the rest of the setup, which includes downloading Android SDK components that are required for development.
	
	if [ -d $STUDIODIR ]; then
		subprint "${YELLOW}preious configuration detected ${NO_COLOR}- refusing to add PATH lines to ~/.profile"
	else

		# TODO: cleanup android mess

		# this fixes license bullshit
		subprint "updating licenses"
		# If this does not work (maybe because android home is set to lalis path, not user path),
		# you can substitute "sdkmanager" for ./Android/Sdk/Tools/bin/sdkmanager
		yes | sudo sdkmanager --licenses

		# this installs adb to address the ".bin/sh: 0 adb not found" situation
		stepapt adb

		## Add Android to system path
		# https://askubuntu.com/questions/60218/how-to-add-a-directory-to-the-path
		# Adicionar

		subprint "updating path"	
	  echo "# Added Automatically: Android Dev Paths"	
		echo "export ANDROID_HOME=\$HOME/Android/Sdk"           >> $dotprofile
		echo "export PATH=\$PATH:\$ANDROID_HOME/tools"          >> $dotprofile
		echo "export PATH=\$PATH:\$ANDROID_HOME/tools/bin"      >> $dotprofile
		echo "export PATH=\$PATH:\$ANDROID_HOME/platform-tools" >> $dotprofile
		echo "export PATH=\$PATH:\$ANDROID_HOME/emulator"       >> $dotprofile
		# /etc/environment is also a possibility for permanently saving these directories for all users
		# sudo likes this

		# Enable usb browser console debugging
		#echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", MODE="0666", GROUP="plugdev"' | sudo tee /etc/udev/rules.d/51-android-usb.rules
		echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", MODE="0666", GROUP="plugdev"' >> "/etc/udev/rules.d/51-android-usb.rules"
		sayok
	fi
fi

if $ELM && ! command -v "lein"; then
    sayname "elm"
    bin=$temp/elm-bin.tar.gz
    source="https://github.com/elm/compiler/releases/download/0.19.0/binaries-for-linux.tar.gz"
    
    stepcurl $source $bin 
    steptar $bin /usr/local/bin/
    sayok
fi
saydone

# Libraries
section "Setting up libraries"
if $SFML || $CRSFML; then
    sayname "crsfml"
    stepapt "\
    libsfml-audio2.4\
    libsfml-dev\
    libsfml-graphics2.4\
    libsfml-network2.4\
    libsfml-system2.4\
    libsfml-window2.4"
    sayok
fi

# crsfml
if $SDL; then
    sayname "sdl"
    #libsdl2.2debian\

    stepapt "\
    libsdl2-2.0-0\
    libsdl2-dev\
    libsdl2-image-2.0-0\
    libsdl2-image-dev\
    libsdl2-mixer-2.0-0\
    libsdl2-mixer-dev\
    libsdl2-ttf-2.0-0\
    libsdl2-ttf-dev"
    sayok
fi

# TODO: We gotta fix this
if $CRSFML; then
    sayname "crsfml" 
    if ! command -v cmake; then
      stepapt "cmake"
    fi
    # go on with https://github.com/oprypin/crsfml
    cd "$dir" || exit
    sayok
fi

if $LSP; then
  if $CRYSTAL; then
    cd $temp || exit
    git clone https://github.com/crystal-lang-tools/scry.git
    cd scry || exit
    shards build -v
  fi
fi
