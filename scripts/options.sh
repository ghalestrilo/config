#!/bin/bash

# Setup Control
listpreset() {
    name="($1) ${2#"$data/presets/"}"
    sayname "${name%".sh"}\n"
    savetext ""
}

preset="$data/presets/basic.sh"
options="$temp/options.sh"
i=1
j=0


echo "Would you like to use a preset?"
for f in $data/presets/*; do
    listpreset $i $f
    i=$((i+1))
done

savetext "Choice: " 
read -p "Choice: " j

i=1
for f in $data/presets/*; do
    if [ $j = $i ]; then
        preset="$f"
        break
    fi
    i=$((i+1))
done

subprint $preset\n
cat "$preset" > "$options"
nano "$options"

. "$options"