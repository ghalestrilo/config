#!/bin/bash

. ./scripts/dirs.sh
. ./scripts/defs.sh
. ./scripts/flags.sh "$@"

if $reverse
then yellow " Loading stored configuration"
else yellow " Storing current configuration"
fi

echo
. ./scripts/configs.sh
saydone
