#!/bin/bash
# Directories
export null=/dev/null
export dir="$PWD"
export homedir="$HOME"

export scripts="$dir/scripts"
export data="$dir/data"
export temp="$dir/temp"
export systemconfig="$homedir/.config"

# Error Handling
export tmplog="$temp/tmplog"
export errlog="$temp/errlog"



# Config Folders
export musescoreconfig=$systemconfig/MuseScore
export nvimconfig=$systemconfig/nvim
export kakouneconfig=$systemconfig/kak
export kaklspconfig=$systemconfig/kak-lsp
export codeconfig=$systemconfig/Code/User
export i3config=$systemconfig/i3
export supercolliderconfig=$systemconfig/SuperCollider
export tidalconfig=$systemconfig/tidal
