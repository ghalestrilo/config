#!/bin/bash
# Function definitions and terminal constants

# Error Reporting
errcount=0
sectioncounter=0

# Text
RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NO_COLOR='\033[0m'
CLEAR_LINE='\r\033[K'
DOT=·

# DIVLINE="$RED | $NO_COLOR"

export reverse=false

# -- Printing Level
CURRENT_TEXT=''
subprint() { printf "${CLEAR_LINE}${CURRENT_TEXT} $1"; }
savetext() { CURRENT_TEXT=$1; }

# -- Colors
green()  { printf "${GREEN} $* ${NO_COLOR}"; }
yellow() { printf "${YELLOW}$* ${NO_COLOR}"; }
red()    { printf "${RED}\ \ $* ${NO_COLOR}"; }

# -- Installation Steps
sayname() { 
    savetext "${CLEAR_LINE} ${BLUE}${DOT}${NO_COLOR} $1:"
    printf "${CURRENT_TEXT}";
}

sayok() {
  subprint "${GREEN}ok\n${NO_COLOR}";
}

sayerror() {
  errcount=$((errcount+1))
  subprint "${RED}error\n${NO_COLOR}";
  cat $tmplog >> $errlog
}

# -- Section Ceremony
section() {
  sectioncounter=$((sectioncounter+1))
  yellow "$sectioncounter. $1\n" 
}

saydone() {
  green " Done!\n \n";
}


# Functions

# Generic Step
# $1: Command
# $2: Message
step() {
  subprint "$2"
  if ! $(eval $1 > $tmplog); then
    sayerror
    printf "   ${YELLOW}${DOT}${NO_COLOR} command ${YELLOW}$1${NO_COLOR} failed\n"
    printf "   ${YELLOW}${DOT}${NO_COLOR} this has been logged\n \n"
    return 1
  fi

  return 0
}

# Curl download
# $1: URL
# $2: Output
stepcurl(){
  cmd="curl -s -fLo \"$2\" --create-dirs \"$1\""
  step "$cmd" "downloading $2"
}

# Copy file to folder
# $1: source
# $2: destination
stepcopy(){
  step "mkdir -p $2"      "creating directory: $2"
  step "cp -r \"$1\" \"$2\"" "copying $1 > $2"
}

# Install via dpkg
# $1: deb package
stepdpkg(){
  cmd="dpkg -i \"$1\""
  step "$cmd" "installing debian package ($1)"
}

# Install via apt-get
# $1: apt package name
# $2: informative name
stepapt(){
  cmd="apt-get install -y $1"
  # sayname "$2"
	step "$cmd" "$cmd"
}

tryapt(){
  cmd="apt-get install -y $1"
  sayname "$2"
  if step "$cmd" "$cmd" ; then
    sayok;
	fi
}
