#!/bin/bash

if ! command -v add-apt-repository; then
	apt-get install software-properties-common
fi

# MuseScore
if $MUSESCORE && ! command -v musescore > $null; then
	add-apt-repository -y ppa:mscore-ubuntu/mscore-stable > $null
fi

# Neovim
if $NEOVIM && ! command -v neovim > $null; then
	add-apt-repository -y ppa:neovim-ppa/stable > $null
fi

# Syncthing 
if $SYNCTHING; then
	curl -s https://syncthing.net/release-key.txt \
	    | sudo apt-key add -                                           
	echo "deb https://apt.syncthing.net/ syncthing stable" \
	    | sudo tee /etc/apt/sources.list.d/syncthing.list
fi

# crystal
if $I_CRYSTAL; then
  curl -sSL https://dist.crystal-lang.org/apt/setup.sh      | sudo bash
  curl -sL "https://keybase.io/crystal/pgp_keys.asc"        | sudo apt-key add -
  echo "deb https://dist.crystal-lang.org/apt crystal main" | sudo tee /etc/apt/sources.list.d/crystal.list
fi

# node
if $I_NODE; then
	apt-key adv --keyserver keyserver.ubuntu.com --recv 68576280
	apt-add-repository "deb https://deb.nodesource.com/node_10.x $(lsb_release -sc) main"
fi

if $TIDAL; then
  sudo add-apt-repository ppa:supercollider/ppa
fi



# update
apt-get update -y            > $null;
apt-get autoremove -y        > $null;
apt-get --fix-broken install > $null;
saydone

